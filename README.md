# Vending Machine

## Object Detection

A neural network is used to identify the bounding box around the pull_tab and the closed_flap/open_flap. These are converted into angles and control signals are then passed on to the Arduino to rotate the can.

## Arduino

Control signals are passed from the host computer to the Arduino in order to control rotation of the cans via mounted stepper motor and the actuation of the can opening mechanism.

Stepper Motors: [4118M-02-07RO NEMA 17 stepper motors](https://www.alltronics.com//mas_assets/acrobat/28M056.pdf)

Stepper Drivers: [A4983 Stepper Motor Drivers](https://www.pololu.com/product/1201)

## User Interface

The interface uses React Native.