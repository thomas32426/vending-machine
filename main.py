#!/usr/bin/env python
from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *

import os
import sys
import time
import datetime
import argparse

from PIL import Image

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable
import torch.nn.functional as F

import math

import numpy as np
import cv2


def detect_boxes(image, model, scale_factor, y_offset):
   
    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor
    
    image = Variable(image.type(Tensor))

    # Get detections
    with torch.no_grad():
        detections = model(image)
        detections = non_max_suppression(detections, conf_thres, nms_thres)

    if detections[0] is not None:
        dets = detections[0].tolist()
        for detection in dets:
            detection[0] = int(detection[0] * scale_factor)
            detection[1] = int(detection[1] * scale_factor - y_offset)
            detection[2] = int(detection[2] * scale_factor)
            detection[3] = int(detection[3] * scale_factor - y_offset)

    else:
        dets = detections[0]
    
    return dets


def find_offsets(frame_width, frame_height, img_size):
    
    scale_factor = frame_width / img_size # 416*x = 640

    dim_diff = np.abs(frame_height - frame_width)
    y_offset = int(dim_diff // 2)
    
    return scale_factor, y_offset


def draw_boxes(frame, image_detections, scaled_image_dimensions):

    for x1, y1, x2, y2, conf, class_conf, class_pred in image_detections:

        if class_pred == 0:
            color = (0, 255, 0)
        elif class_pred == 1:
            color = (255, 0, 0)
        else:
            color = (0, 0, 255)

        image_with_boxes = cv2.rectangle(frame, (x1, y1), (x2, y2), color, 1)

    return image_with_boxes


def find_centroid(x1, y1, x2, y2):
    return (x2 + x1) // 2, (y2 + y1) // 2


def find_angles(image_detections, frame_width, frame_height):
    
    angles = []

    center_x, center_y = find_centroid(0, 0, frame_width, frame_height)

    for x1, y1, x2, y2, conf, class_conf, class_pred in image_detections:

        pred_centroid_x, pred_centroid_y = find_centroid(x1, y1, x2, y2)
        
        radians = math.atan2(pred_centroid_y - center_y, pred_centroid_x - center_x)
        degrees = math.degrees(radians)
        
        if class_pred == 1:
            calculate_servo_message(radians)

        angles.append([radians, degrees, class_pred, pred_centroid_x, pred_centroid_y])

    return angles


def calculate_servo_message(angle):
    threshold = math.radians(5)

    if abs(angle) < threshold:
        print("Goal!", format(abs(math.degrees(angle)), '.1f'))
    elif angle < 0:
        print("Clockwise", format(abs(math.degrees(angle)), '.1f'))
    else:
        print("Counter-clockwise", format(math.degrees(angle), '.1f'))


def draw_angles(image, image_detections):

    center_x, center_y = find_centroid(0, 0, image.shape[1], image.shape[0])
    
    angles = find_angles(image_detections, image.shape[1], image.shape[0])

    for values in angles:
        if values[2] == 0:
            color = (0, 255, 0)
        elif values[2] == 1:
            color = (255, 0, 0)
        else:
            color = (0, 0, 255)
        
        image = cv2.line(image, (center_x, center_y), (values[3], values[4]), color, 5)

    return image


if __name__ == "__main__":
    cap = cv2.VideoCapture(2)

    # Variables
    image_folder = "data/sample_cans"
    model_def = "config/cans.cfg"
    weights_path = "weights/cans_3000.weights"
    class_path = "config/cans.names"
    conf_thres = 0.4
    nms_thres = 0.4
    batch_size = 1
    n_cpu = 0
    img_size = 416
    checkpoint_model = ""

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Set up model
    model = Darknet(model_def, img_size=img_size).to(device)

    # Load darknet weights
    model.load_darknet_weights(weights_path)
    
    model.eval()  # Set in evaluation mode

    classes = load_classes(class_path)  # Extracts class labels from file

    ret, frame = cap.read()

    scale_factor, y_offset = find_offsets(frame.shape[1], frame.shape[0], img_size)

    # Run the main loop
    while(True):
        # Capture frame-by-frame.
        ret, frame = cap.read()

        # If there is a new frame
        if ret:
            x = torch.from_numpy(frame.transpose(2, 0, 1))
            x = x.unsqueeze(0).float()  # x = (1, 3, H, W) (1, 2, 480, 640)
            
            # Apply letterbox resize
            _, _, h, w = x.size()
            ih, iw = (416, 416)
            dim_diff = np.abs(h - w)
            pad1, pad2 = int(dim_diff // 2), int(dim_diff - dim_diff // 2) # 80, 80
            pad = (pad1, pad2, 0, 0) if w <= h else (0, 0, pad1, pad2)
            x = F.pad(x, pad=pad, mode='constant', value=127.5) / 255.0
            x = F.upsample(x, size=(ih, iw), mode='bilinear') # x = (1, 3, 416, 416)

            image_detections = detect_boxes(x, model, scale_factor, y_offset)
    
            scaled_image_dimensions = (x.shape[-2], x.shape[-1])

            
            # Draw boxes
            if image_detections is not None:
                #image_with_boxes = draw_boxes(frame, image_detections, scaled_image_dimensions)
                image_with_lines = draw_angles(frame, image_detections)
                cv2.imshow('Frame', image_with_lines)
                # Display the resulting frame.
                #cv2.imshow('Frame', image_with_boxes)
            else:
                cv2.imshow('Frame', frame)

        # Break out of the program if we're done.
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()