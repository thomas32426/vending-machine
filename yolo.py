from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *

import os
import sys
import time
import datetime
import argparse

from PIL import Image

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

def detect_boxes(image):
   # Variables
    image_folder = "data/sample_cans"
    model_def = "config/cans.cfg"
    weights_path = "weights/cans_3000.weights"
    class_path = "data/cans/classes.names"
    conf_thres = 0.8
    nms_thres = 0.4
    batch_size = 1
    n_cpu = 0
    img_size = 416
    checkpoint_model = ""

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Set up model
    model = Darknet(model_def, img_size=img_size).to(device)

    # Load darknet weights
    model.load_darknet_weights(weights_path)
    
    model.eval()  # Set in evaluation mode

    classes = load_classes(class_path)  # Extracts class labels from file

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor
    
    image = Variable(image.type(Tensor))
    # for batch_i, (img_paths, input_imgs) in enumerate(dataloader):
    #     # Configure input
    #     input_imgs = )

    # Get detections
    with torch.no_grad():
        detections = model(image)
        detections = non_max_suppression(detections, conf_thres, nms_thres)

        # imgs.extend(img_paths)
        # img_detections.extend(detections)

    # dets = [x.tolist() for x in img_detections]
    
    return detections